# Manage PHP version using Build ARGS
ARG PHP_VERSION=7
ARG DISTRO=alpine
FROM php:${PHP_VERSION}-${DISTRO}

# Install node and other utilities
RUN apk add bash nodejs npm

# Install composer
RUN curl https://getcomposer.org/installer -so composer-setup.php && \
    php composer-setup.php --install-dir=/usr/local/bin --filename=composer && \
    rm composer-setup.php

# Generate release info
RUN echo "{" > /release.json && \
    echo "  \"php\": \"$( php --version | head -1 )\"," >> /release.json && \
    echo "  \"composer\": \"$( su operator -c 'composer --version' )\"," >> /release.json && \
    echo "  \"node\": \"$( node --version )\"," >> /release.json && \
    echo "  \"npm\": \"$( npm --version )\"" >> /release.json && \
    echo "}" >> /release.json