# Erawan testing image

Docker images with tools used to test [Erawan CMS](https://gitlab.com/erawancms/erawan). 
This repository uses [GitLabCI pipelines](https://gitlab.com/erawancms/testing-image/pipelines) to build following
Docker images for different PHP versions.

This image include following components:

 * **[PHP 7](https://php.net//)** - PHP 7 runtime engine
 * **[composer](https://getcomposer.org/)** - used to manage PHP dependencies
 * **[node](https://nodejs.org/)** - used to manage build dependencies
 * **[npm](https://www.npmjs.com/)** - used to manage node dependencies

## Usage

This image is based on [PHP Docker Official Image](https://hub.docker.com/_/php/). 
See base image documentation to get [detailed usage](https://hub.docker.com/_/php/#how-to-use-this-image).

Run PHP interactive shell
 
```shell
$ docker run --ti -rm registry.gitlab.com/erawancms/testing-image
Interactive shell

php > 
```

Run alternative shell

```shell
$ docker run --ti -rm registry.gitlab.com/erawancms/testing-image /bin/bash
bash-4.4#
```

## Available Images

See [GitLab Container Registry](https://gitlab.com/erawancms/testing-image/container_registry) for available images

